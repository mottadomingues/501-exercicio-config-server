package tech.master.turmas;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cursos", fallback = CursoClientFallBack.class)
public interface CursoClient {

    @GetMapping
    public Iterable<Curso> listar();

    @GetMapping("/{id}")
    public Curso buscar(@PathVariable int id);
}
