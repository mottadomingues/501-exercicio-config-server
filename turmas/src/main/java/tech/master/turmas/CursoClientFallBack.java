package tech.master.turmas;

import org.springframework.stereotype.Component;

@Component
public class CursoClientFallBack implements CursoClient {

	@Override
	public Iterable<Curso> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Curso buscar(int id) {
		Curso curso = new Curso();
		curso.setNome("A ser definido");
		return curso;
	}

}
